package auth

import (
	"database/sql"
	"errors"
	"github.com/hashicorp/go-uuid"
	"log"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
)

var usersRepoQueries = []string{
	`CREATE TABLE IF NOT EXISTS users
( 
	id TEXT,
	password TEXT,
	phone_number TEXT,
	phone_number_verified boolean,
	first_name TEXT,
	last_name TEXT,
	gender TEXT,
	date_of_birth TEXT,
	PRIMARY KEY (id),
	UNIQUE (phone_number)
);`,
}

type Store interface {
	Get(id string) (*User, error)

	Create(user *User) (*User, error)

	Update(user *UserUpdate) (*User, error)

	Delete(id string) error

	List() ([]User, error)

	GetByPhone(phoneNumber string) (*User, error)
}

type store struct {
	db *sql.DB
}

func NewUserStore(cfg Config) (Store, error) {
	db, err := getDbConn(getConnString(cfg))
	if err != nil {
		return nil, err
	}
	for _, q := range usersRepoQueries {
		_, err = db.Exec(q)
		if err != nil {
			log.Println(err)
		}
	}

	store := &store{db}

	defaultUserCreation := &User{
		ID:                  "root4",
		Password:            "$2y$10$/ftQ5o8yxL4pSqql.uy3EueW30lNBc5Lx7s0FvT/uc2WN9PFGD/d.",
		PhoneNumber:         "+77073613026",
		PhoneNumberVerified: true,
		Gender:              "male",
	}

	_, err = store.Create(defaultUserCreation)
	if err != nil {
		log.Println("Could not create default user, err: ", err.Error())
	}

	return store, nil
}

func (s *store) Create(user *User) (*User, error) {
	if user.ID != "root4" {
		id, err := uuid.GenerateUUID()
		if err != nil {
			return nil, err
		}
		user.ID = id
	}
	result, err := s.db.Exec(
		"INSERT INTO users (id, password, "+
			"phone_number, phone_number_verified, "+
			"first_name, last_name, "+
			"gender, date_of_birth) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
		user.ID, user.Password, user.PhoneNumber, user.PhoneNumberVerified,
		user.FirstName, user.LastName, user.Gender, user.DateOfBirth)
	if err != nil {
		return nil, err
	}
	n, err := result.RowsAffected()
	if err != nil {
		return nil, err
	}
	if n <= 0 {
		return nil, errors.New("Can not create new user, error: Unknown error ")
	}
	return user, nil
}

func (s *store) Update(user *UserUpdate) (*User, error) {
	q := "UPDATE users SET "
	var parts []string
	var values []interface{}
	cnt := 0

	if user.PhoneNumberVerified != nil {
		cnt++
		parts = append(parts, "phone_number_verified = $"+strconv.Itoa(cnt))
		values = append(values, *user.PhoneNumberVerified)
	}
	if user.Password != nil {
		cnt++
		parts = append(parts, "password = $"+strconv.Itoa(cnt))
		values = append(values, *user.Password)
	}
	if user.PhoneNumber != nil {
		cnt++
		parts = append(parts, "phone_number = $"+strconv.Itoa(cnt))
		values = append(values, *user.PhoneNumber)
	}
	if user.FirstName != nil {
		cnt++
		parts = append(parts, "first_name = $"+strconv.Itoa(cnt))
		values = append(values, *user.FirstName)
	}
	if user.LastName != nil {
		cnt++
		parts = append(parts, "last_name = $"+strconv.Itoa(cnt))
		values = append(values, *user.LastName)
	}
	if user.Gender != nil {
		cnt++
		parts = append(parts, "gender = $"+strconv.Itoa(cnt))
		values = append(values, *user.Gender)
	}
	if user.DateOfBirth != nil {
		cnt++
		parts = append(parts, "date_of_birth = $"+strconv.Itoa(cnt))
		values = append(values, *user.DateOfBirth)
	}

	if len(parts) <= 0 {
		return nil, Error{400, ErrNothingToUpdate}
	}
	cnt++
	q = q + strings.Join(parts, " , ") + " WHERE id = $" + strconv.Itoa(cnt)
	values = append(values, user.ID)
	result, err := s.db.Exec(
		q,
		values...,
	)
	if err != nil {
		return nil, err
	}
	n, err := result.RowsAffected()
	if err != nil {
		return nil, err
	}
	if n <= 0 {
		return nil, Error{404, ErrUserNotFound}
	}
	return s.Get(user.ID)
}

func (s *store) Delete(id string) error {
	result, err := s.db.Exec("DELETE FROM users WHERE id = $1", id)
	if err != nil {
		return err
	}
	n, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if n <= 0 {
		return Error{404, ErrUserNotFound}
	}
	return nil
}

func (s *store) Get(id string) (*User, error) {
	item := &User{}
	err := s.db.QueryRow(
		"SELECT id, password, phone_number, phone_number_verified, "+
			"first_name, last_name, gender, date_of_birth FROM users WHERE id = $1 LIMIT 1", id).
		Scan(&item.ID, &item.Password, &item.PhoneNumber, &item.PhoneNumberVerified,
			&item.FirstName, &item.LastName, &item.Gender, &item.DateOfBirth)
	if err == sql.ErrNoRows {
		return nil, Error{404, ErrUserNotFound}
	} else if err != nil {
		return nil, err
	}
	return item, nil
}

func (s *store) List() ([]User, error) {
	var items []User
	rows, err := s.db.Query("SELECT id, password, phone_number, phone_number_verified, " +
		"first_name, last_name, gender, date_of_birth FROM users")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		item := User{}
		err = rows.Scan(&item.ID, &item.Password, &item.PhoneNumber, &item.PhoneNumberVerified,
			&item.FirstName, &item.LastName, &item.Gender, &item.DateOfBirth)
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}
	return items, nil
}

func (s *store) GetByPhone(phoneNumber string) (*User, error) {
	item := &User{}
	err := s.db.QueryRow(
		"SELECT id, password, phone_number, phone_number_verified, "+
			"first_name, last_name, gender, date_of_birth FROM users WHERE phone_number = $1 LIMIT 1", phoneNumber).
		Scan(&item.ID, &item.Password, &item.PhoneNumber, &item.PhoneNumberVerified,
			&item.FirstName, &item.LastName, &item.Gender, &item.DateOfBirth)
	if err == sql.ErrNoRows {
		return nil, Error{404, ErrUserNotFound}
	} else if err != nil {
		return nil, err
	}
	return item, nil
}
