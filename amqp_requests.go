package auth

import (
	"encoding/json"
	"github.com/djumanoff/amqp"
	"gitlab.com/wtaxi/helper"
)

type AmqpRequests struct {
	clt amqp.Client
}

func NewAmqpRequests(clt amqp.Client) *AmqpRequests {
	return &AmqpRequests{clt}
}

//func (r *AmqpRequests) SendSMS(sms *SMS) error {
//	_, err := r.amqpCall(sms, "send.sms")
//	return err
//}

func (r *AmqpRequests) amqpCall(data interface{}, path string) (interface{}, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	response, err := r.clt.Call(path, amqp.Message{
		Body: jsonData,
	})
	if err != nil {
		return nil, err
	}
	amqpResp := &helper.AMQPResponse{}
	if err := json.Unmarshal(response.Body, amqpResp); err != nil {
		return nil, err
	}
	if amqpResp.Error != nil {
		return nil, amqpResp.Error
	}
	return amqpResp.Body, nil
}
