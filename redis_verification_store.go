package auth

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis"
	"strconv"
)

type RedisConfig struct {
	Host     string
	Password string
	Port     int
	DB       int
	Ctx      context.Context
}

type VerificationStore struct {
	cfg RedisConfig
	clt *redis.Client
	ctx context.Context
}

func NewVerificationStore(cfg RedisConfig) (*VerificationStore, error) {
	strconv.Itoa(cfg.DB)

	client := redis.NewClient(&redis.Options{
		Addr:     cfg.Host + ":" + strconv.Itoa(cfg.Port),
		Password: cfg.Password, // no password set
		DB:       cfg.DB,       // use default DB
	})

	_, err := client.Ping(cfg.Ctx).Result()
	if err != nil {
		return nil, err
	}
	return &VerificationStore{cfg, client, cfg.Ctx}, nil
}

func (store *VerificationStore) Get(token string) (*PhoneVerification, error) {
	result, err := store.clt.Get(store.ctx, "phone_verification:"+token).Bytes()
	if err != nil {
		return nil, err
	}
	item := &PhoneVerification{}
	if err := json.Unmarshal(result, item); err != nil {
		return nil, err
	}
	return item, nil
}

func (store *VerificationStore) Set(item *PhoneVerification) error {
	_, err := store.clt.Set(store.ctx, "phone_verification:"+item.Token, item, item.TTL).Result()
	if err != nil {
		return err
	}
	return nil
}

func (store *VerificationStore) Delete(token string) error {
	_, err := store.clt.Del(store.ctx, "phone_verification:"+token).Result()
	return err
}
