package auth

import (
	"fmt"
	"net/http"
)

type SMSSenderService interface {
	Send(sms *SMS) error
}

type smsSenderService struct {
	clt   http.Client
	login string
	pass  string
}

func NewSmsService(clt http.Client, login, pass string) *smsSenderService {
	return &smsSenderService{
		clt:   clt,
		login: login,
		pass:  pass,
	}
}

func (svc *smsSenderService) Send(sms *SMS) error {
	url := fmt.Sprintf("https://smsc.kz/sys/send.php?login=%s&psw=%s&phones=%s&mes=%s", svc.login, svc.pass, sms.Phone, sms.Message)
	_, err := svc.clt.Get(url)
	return err
}
