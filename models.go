package auth

import "time"

type User struct {
	ID                  string `json:"id,omitempty"`
	Password            string `json:"password,omitempty"`
	PhoneNumber         string `json:"phone_number,omitempty"`
	PhoneNumberVerified bool   `json:"phone_number_verified,omitempty"`
	FirstName           string `json:"first_name,omitempty"`
	LastName            string `json:"last_name,omitempty"`
	Gender              string `json:"gender,omitempty"`
	DateOfBirth         string `json:"date_of_birth,omitempty"`
}

type UserUpdate struct {
	ID                  string  `json:"id,omitempty"`
	Password            *string `json:"password,omitempty"`
	PhoneNumber         *string `json:"phone_number,omitempty"`
	PhoneNumberVerified *bool   `json:"phone_number_verified,omitempty"`
	FirstName           *string `json:"first_name,omitempty"`
	LastName            *string `json:"last_name,omitempty"`
	Gender              *string `json:"gender,omitempty"`
	DateOfBirth         *string `json:"date_of_birth,omitempty"`
}

type PhoneVerification struct {
	PhoneNumber string        `json:"phone_number"`
	Code        string        `json:"code"`
	Token       string        `json:"token"`
	Status      string        `json:"status"`
	UserID      string        `json:"user_id,omitempty"`
	TTL         time.Duration `json:"ttl"`
}

type SignInResponse struct {
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
}

type TokenResponse struct {
	Token string `json:"token"`
}

type SignUpRequest struct {
	Password string `json:"password"`
}

type SMS struct {
	Phone   string `json:"phone"`
	Message string `json:"message"`
}

type PhoneVerifyResponse struct {
	VerificationToken string `json:"verification_token"`
}

type ResponseOK struct {
	Message string `json:"message"`
}
