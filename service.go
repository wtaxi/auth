package auth

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/hashicorp/go-uuid"
	"gitlab.com/wtaxi/helper"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"time"
)

type Service struct {
	userStore         Store
	amqpRequests      AmqpRequests
	verificationStore VerificationStore
	smsSenderService  SMSSenderService
}

func NewAuthService(userStore *Store, requests *AmqpRequests, verificationStore *VerificationStore, service SMSSenderService) *Service {
	return &Service{
		userStore:         *userStore,
		amqpRequests:      *requests,
		verificationStore: *verificationStore,
		smsSenderService: service,
	}
}

func (svc *Service) SignUpSend(phone string) (*PhoneVerifyResponse, error) {
	if err := isPhoneFormat(phone); err != nil {
		return nil, err
	}

	existingUser, _ := svc.userStore.GetByPhone(phone)
	if existingUser != nil {
		return nil, NewError(403, "user with phone: "+phone+" already exist")
	}

	code := codeGenerator(6)
	token, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: phone,
		Code:        code,
		Token:       token,
		Status:      "sign_up_send",
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	err = svc.smsSenderService.Send(&SMS{
		Phone:   phone,
		Message: code,
	})
	if err != nil {
		return nil, err
	}
	return &PhoneVerifyResponse{VerificationToken: token}, nil
}

func (svc *Service) SignUpVerify(token, code string) (*PhoneVerifyResponse, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "sign_up_send" {
		return nil, ErrInvalidVerificationToken
	}

	if item.Code != code {
		return nil, ErrInvalidCode
	}

	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: item.PhoneNumber,
		Code:        item.Code,
		Token:       item.Token,
		Status:      "sign_up_verified",
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	return &PhoneVerifyResponse{token}, nil
}

func (svc *Service) SignUp(token, password string) (*TokenResponse, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "sign_up_verified" {
		return nil, ErrInvalidVerificationToken
	}

	user := &User{
		PhoneNumber:         item.PhoneNumber,
		PhoneNumberVerified: true,
	}
	newPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, Error{500, err}
	}
	user.Password = string(newPass)

	user, err = svc.userStore.Create(user)
	if err != nil {
		return nil, err
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}

	tkn, err := helper.GenerateToken(&helper.Claims{
		PhoneNumber: user.PhoneNumber,
		StandardClaims: jwt.StandardClaims{
			Subject: user.ID,
		},
	})
	return &TokenResponse{tkn}, nil
}

func (svc *Service) SignIn(cmd *SignInResponse) (*TokenResponse, error) {
	user, err := svc.userStore.GetByPhone(cmd.PhoneNumber)
	if err != nil {
		return nil, err
	}
	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(cmd.Password)); err != nil {
		return nil, Error{401, err}
	}

	tkn, err := helper.GenerateToken(&helper.Claims{
		PhoneNumber: user.PhoneNumber,
		StandardClaims: jwt.StandardClaims{
			Subject: user.ID,
		},
	})
	return &TokenResponse{tkn}, nil
}

func (svc *Service) RefreshToken(claims *helper.Claims) (*TokenResponse, error) {
	tkn, err := helper.GenerateToken(claims)
	if err != nil {
		return nil, err
	}
	return &TokenResponse{Token: tkn}, nil
}

func (svc *Service) UpdatePassword(password, id string) (*ResponseOK, error) {
	newPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, Error{500, err}
	}
	pass := string(newPass)

	_, err = svc.userStore.Update(&UserUpdate{ID: id, Password: &pass})
	if err != nil {
		return nil, err
	}

	return &ResponseOK{"Password updated"}, nil
}

func (svc *Service) UpdatePhoneSend(phone, id string) (*PhoneVerifyResponse, error) {
	if err := isPhoneFormat(phone); err != nil {
		return nil, err
	}

	existingUser, _ := svc.userStore.GetByPhone(phone)
	if existingUser != nil {
		return nil, NewError(403, "user with phone: "+phone+" already exist")
	}

	code := codeGenerator(6)
	token, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: phone,
		Code:        code,
		Token:       token,
		Status:      "update_phone_send",
		UserID:      id,
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	err = svc.smsSenderService.Send(&SMS{
		Phone:   phone,
		Message: code,
	})
	if err != nil {
		return nil, err
	}
	return &PhoneVerifyResponse{VerificationToken: token}, nil
}

func (svc *Service) UpdatePhone(token, code string) (*ResponseOK, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "update_phone_send" {
		return nil, ErrInvalidVerificationToken
	}

	if item.Code != code {
		return nil, ErrInvalidCode
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}

	isVerified := true
	_, err = svc.userStore.Update(&UserUpdate{
		ID:                  item.UserID,
		PhoneNumber:         &item.PhoneNumber,
		PhoneNumberVerified: &isVerified,
	})
	if err != nil {
		return nil, err
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}
	return &ResponseOK{"Phone Updated"}, nil
}

func (svc *Service) ResetPasswordSend(phone string) (*PhoneVerifyResponse, error) {
	if err := isPhoneFormat(phone); err != nil {
		return nil, err
	}

	existingUser, err := svc.userStore.GetByPhone(phone)
	if err != nil {
		return nil, err
	}

	code := codeGenerator(6)
	token, err := uuid.GenerateUUID()
	if err != nil {
		return nil, err
	}
	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: phone,
		Code:        code,
		Token:       token,
		Status:      "reset_password_send",
		UserID:      existingUser.ID,
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	err = svc.smsSenderService.Send(&SMS{
		Phone:   phone,
		Message: code,
	})
	if err != nil {
		return nil, err
	}
	return &PhoneVerifyResponse{VerificationToken: token}, nil
}

func (svc *Service) ResetPasswordVerify(token, code string) (*PhoneVerifyResponse, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "reset_password_send" {
		return nil, ErrInvalidVerificationToken
	}

	if item.Code != code {
		return nil, ErrInvalidCode
	}

	err = svc.verificationStore.Set(&PhoneVerification{
		PhoneNumber: item.PhoneNumber,
		Code:        item.Code,
		Token:       item.Token,
		Status:      "reset_password_verified",
		UserID:      item.UserID,
		TTL:         3 * time.Minute,
	})
	if err != nil {
		return nil, err
	}

	return &PhoneVerifyResponse{token}, nil
}

func (svc *Service) ResetPassword(token, password string) (*ResponseOK, error) {
	item, err := svc.verificationStore.Get(token)
	if err != nil {
		return nil, err
	}
	if item.Status != "reset_password_verified" {
		return nil, ErrInvalidVerificationToken
	}

	newPass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	password = string(newPass)

	user := &UserUpdate{
		ID:       item.UserID,
		Password: &password,
	}
	_, err = svc.userStore.Update(user)
	if err != nil {
		return nil, err
	}

	err = svc.verificationStore.Delete(token)
	if err != nil {
		return nil, err
	}

	return &ResponseOK{"Password reset finished"}, nil
}

func (svc *Service) Welcome(username string) (interface{}, error) {
	return struct {
		Message string `json:"message"`
	}{"Welcome, " + username}, nil
}

func (svc *Service) List() ([]User, error) {
	return nil, nil
	//return srv.UserStore.List()
}

const letterBytes = "0123456789"

func codeGenerator(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func isPhoneFormat(phone string) error {
	if len(phone) != 12 && phone[0] != '+' {
		return ErrWrongPhoneFormat
	}
	return nil
}
