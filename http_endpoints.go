package auth

import (
	"github.com/gorilla/mux"
	"gitlab.com/wtaxi/helper"
	"net/http"
)

type HttpEndpointFactory struct {
	Service *Service
}

func (fac *HttpEndpointFactory) ListEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	return nil, nil
}

func (fac *HttpEndpointFactory) SignInEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	signIn := &SignInResponse{}
	if err := helper.UnmarshalJSON(r, signIn); err != nil {
		return nil, err
	}
	resp, err := fac.Service.SignIn(signIn)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) WelcomeEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	claims := r.Context().Value("props").(*helper.Claims)
	resp, err := fac.Service.Welcome(claims.PhoneNumber)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) RefreshTokenEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	claims := r.Context().Value("props").(*helper.Claims)
	resp, err := fac.Service.RefreshToken(claims)
	if err != nil {
		return nil, err
	}

	return &helper.Response{
		Body:       resp,
		StatusCode: 200,
	}, nil
}

func (fac *HttpEndpointFactory) SignUpEndpoint(w http.ResponseWriter, r *http.Request) (*helper.Response, error) {
	vars := mux.Vars(r)
	response := &helper.Response{
		StatusCode: 200,
	}
	if r.Method == "GET" {
		phone := vars["phone"]
		if phone == "" {
			return nil, Error{400, ErrPhoneNotProvided}
		}

		resp, err := fac.Service.SignUpSend(phone)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "POST" {
		code := vars["code"]
		token := vars["verification_token"]
		if code == "" {
			return nil, ErrInvalidCode
		}
		if token == "" {
			return nil, ErrInvalidVerificationToken
		}
		resp, err := fac.Service.SignUpVerify(token, code)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else if r.Method == "PUT" {
		token := vars["verification_token"]
		if token == "" {
			return nil, ErrInvalidVerificationToken
		}
		signUp := &SignUpRequest{}
		if err := helper.UnmarshalJSON(r, signUp); err != nil {
			return nil, err
		}
		resp, err := fac.Service.SignUp(token, signUp.Password)
		if err != nil {
			return nil, err
		}
		response.Body = resp
	} else {
		return nil, NewError(405, "Method not allowed")
	}
	return response, nil
}